# -*- coding: utf-8 -*-
"""
This script helps in managing car repairs, it contains basic information
about parts durability, enables adding part change in your own car
and shows report how long your part will last, as approximation.
"""
import argparse
import datetime
import os
import json
import sys


class CarElement():
    """
    Describes part and its expected durability.
    """
    QUALITIES = ["good", "medium", "cheap"]
    QUALITIES_MULTIPLIER = [1.2, 1.0, 0.8]

    def __init__(self, name, time, kilometers):
        self.name = name
        self.change_after_months = time
        self.change_after_km = kilometers

    def __str__(self):
        return_string = f"Name: {self.name}, Change after {self.change_after_months / 12} years"
        if self.change_after_km != -1:
            return_string += f" or change after {self.change_after_km} km"
        return return_string

    def print_status(self, change_date, part_changed_at_km, current_odometer_value, quality):
        """Prints part durability status to standard output."""
        part_multiplier = CarElement.QUALITIES_MULTIPLIER[CarElement.QUALITIES.index(quality)]
        change_date_as_date = datetime.datetime(change_date[0], change_date[1], change_date[2])
        due_date = change_date_as_date + datetime.timedelta(days=self.change_after_months * 31 * part_multiplier)
        now_date = datetime.datetime.now()
        if due_date >= now_date:
            print(f'{self.name} is fine. Change it till {due_date}')
        elif part_changed_at_km != -1 and self.change_after_km != -1:
            part_kilometer_usage = current_odometer_value - part_changed_at_km
            part_km_durability = self.change_after_km * part_multiplier
            kilometers_left = part_km_durability - part_kilometer_usage
            if kilometers_left > 0:
                print(f'{self.name} should be already changed {-kilometers_left} kilometers ago')
        else:
            print(f'{self.name} be already changed on {due_date}')

def create_parts_database():
    """Creates supported car parts and saves them to file."""
    parts_dict = []
    parts_dict.append(CarElement("all_season_tire", 6 * 12, 50000))
    parts_dict.append(CarElement("oil_and_filter", 12, 15000))
    parts_dict.append(CarElement("air_filter", 18, 30000))
    parts_dict.append(CarElement("fuel_filter", 18, 20000))
    parts_dict.append(CarElement("cabin_filter", 18, 30000))
    parts_dict.append(CarElement("acumulator", 6 * 12, -1))
    parts_dict.append(CarElement("catalisator", 7 * 12, 100000))
    parts_dict.append(CarElement("cabin_filter", 18, 15000))
    parts_dict.append(CarElement("flexible_joint", 5 * 12, 40000))
    parts_dict.append(CarElement("fabia_stabilizer_bar", 3 * 12, 40000))  # universal unknown
    parts_dict.append(CarElement("toothed_belt", 5 * 12, 120000))
    parts_dict.append(CarElement("both_suspension_springs", 8 * 12, 120000))
    parts_dict.append(CarElement("tie_rod_end", 8 * 12, 100000))
    parts_dict.append(CarElement("shock_absorber", 6 * 12, 70000))
    save_parts(parts_dict)


def save_parts(parts: list):
    """Saves parts to file in json format."""
    json_list = []
    for part in parts:
        json_list.append(part.__dict__)
    with open('parts.json', 'w') as outfile:
        json.dump(json_list, outfile)


def load_parts():
    """Loads supported parts from file to list."""
    if not os.path.exists('parts.json'):
        create_parts_database()
    with open('parts.json', 'r') as infile:
        data = json.load(infile)
        real_data = []
        for part in data:
            real_data.append(CarElement(part["name"], part["change_after_months"], part["change_after_km"]))
    return real_data


def load_car_data():
    """Loads information about car parts, provided by user."""
    file_name = "data.json"
    if os.path.exists(file_name):
        with open(file_name) as json_file:
            data = json.load(json_file)
    else:
        data = []
    return data


def save_car_data(car_data):
    """Saves information about car parts, provided by user."""
    file_name = "data.json"
    with open(file_name, 'w') as outfile:
        json.dump(car_data, outfile)



def add_part(parts_data: list, car_data: list):
    """Adds changed part to car data"""
    for part in parts_data:
        print(parts_data.index(part) + 1, str(part))
    input_val = input("Provide index of a part")
    index = int(input_val)
    chosen_part = parts_data[index - 1]
    print('Enter a year, -1 in unknown\n')
    year = get_int_value(datetime.datetime.today().year, -1)
    print('Enter a month, -1 in unknown\n')
    month = get_int_value(12, -1)
    print('Enter a day, -1 in unknown\n')
    day = get_int_value(12, -1)
    print('Enter currect odometer value (kilometers), -1 in unknown\n')
    kilometers = get_int_value(None, -1)

    quality = "None"
    while quality not in CarElement.QUALITIES:
        input_val = input(f"Enter quality of part, must be one of {'/'.join(CarElement.QUALITIES)}\n")
        quality = input_val
    # Set default values
    if day == -1:
        day = 15
    if month == -1:
        month = 6
    date_of_change = (year, month, day)
    existing_parts = [x for x in car_data if x[0] is chosen_part.name]
    for existing_part in existing_parts:
        car_data.remove(existing_part)
    car_data.append((chosen_part.name, date_of_change, kilometers, quality))


def get_int_value(lower_than_constraint=None, higher_than_constraint=None):
    """Gets interger value inbetween constraints"""
    if lower_than_constraint is None:
        lower_than_constraint = sys.maxsize
    if higher_than_constraint is None:
        higher_than_constraint = 0
    value = None
    while value is None or not higher_than_constraint <= value <= lower_than_constraint:
        input_val = input()
        try:
            value = int(input_val)
        except ValueError:
            print("That's not a number!")
    return value


def create_report(parts_data: list, car_data: list, odometer_value):
    """Creates report about parts in a car - when to change it or after how many kilometers."""
    for part_name, change_date, part_changed_at_km, quality in car_data:
        part = [part for part in parts_data if part.name == part_name][0]
        part.print_status(change_date, part_changed_at_km, odometer_value, quality)


def main():
    """Main function."""
    argument_parser = argparse.ArgumentParser(description='Car monitor')
    argument_parser.add_argument('--print_report', action='store_true', default=False,
                                 help='Only prints report to standard output and quits.')
    args = argument_parser.parse_args()
    parts_data = load_parts()
    car_data = load_car_data()
    if args.print_report:
        odometer_value = -1 #unknown
        create_report(parts_data, car_data, odometer_value)
    else:
        while True:
            input_data = input("What do you want to do? [q - exit] [a - add part change]\n")
            if input_data == "q":
                break
            if input_data == "a":
                add_part(parts_data, car_data)
            save_car_data(car_data)
        odometer_value = int(input("Provide current odometer value (kilometers) -1 if unknown\n"))
        create_report(parts_data, car_data, odometer_value)

if __name__ == "__main__":
    main()
