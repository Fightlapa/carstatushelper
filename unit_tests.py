# -*- coding: utf-8 -*-
"""
Unit tests for car_status_monitor module.
"""
from unittest.mock import patch
import unittest

import car_status_monitor

#pylint: disable=missing-function-docstring

class FabiaMonitorUnitTests(unittest.TestCase):
    """
    Unit tests for car_status_monitor module.
    """
    def test_adding_part_change_should_override_previous(self):
        parts_data = car_status_monitor.load_parts()
        user_input = [
            '1', #index of a part
            '2000', #year
            '-1', #default month
            '-1', #default day
            '-1', #odometer
            car_status_monitor.CarElement.QUALITIES[0] #any quality
        ]
        car_data = []
        with patch('builtins.input', side_effect=user_input):
            car_status_monitor.add_part(parts_data, car_data)
        with patch('builtins.input', side_effect=user_input):
            car_status_monitor.add_part(parts_data, car_data)
        self.assertEqual(len(car_data), 1)


if __name__ == '__main__':
    unittest.main()
